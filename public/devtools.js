
const log = (...args) => chrome.devtools.inspectedWindow.eval(`
console.log(...${JSON.stringify(args)});
`);

chrome.devtools.panels.create('Data Decrypt', 'tab-icon.png', 'index.html', () => {
	console.log('user switched to this panel');
});


let allDataList = []

const sendData = (source = "devtools", data) => {
	data && chrome.runtime.sendMessage({
		source,
		data
	});
}

async function handleData(res) {
	try {
		// const [{
		//   // 请求的类型，查询参数，以及url
		//   request: { method, postData, queryString, url, headers },
		//   response,
		//   // 该方法可用于获取响应体
		//   getContent,
		// }] = args;
		const {
			request: { method, postData, queryString, url, headers },
			response,
			// 该方法可用于获取响应体
			getContent,
		} = res

		if (method !== "OPTIONS") {
			// 将callback转为await promise
			// warn: content在getContent回调函数中，而不是getContent的返回值
			const headers = response?.headers ?? []
			const _Encrypt = headers.find(item => item.name.toLowerCase() === 'encrypt')
			if (_Encrypt && _Encrypt.value === 'true') {

				const content = await new Promise((res, rej) => getContent(res));
				const _responseObj = JSON.parse(content)
				const rawData = _responseObj.data
				const decryData = decrypt(rawData)
				const _tempData = {
					url,
					method,
					rawData,
					decryData
				}
				allDataList.push({ ..._tempData })
				sendData('devtools-new', { ..._tempData })
			}

		}
	} catch (err) {
		log(err.stack || err.toString());
	}
}


/* chrome.runtime.onConnect.addListener(channel => {
	// 如果连接不是来源于当前tab页，就不使用它。
	if (channel.name !== chrome.devtools.inspectedWindow.tabId.toString()) {
		return;
	}

	// 作用域中留下了当前tab页的Panel所建立的连接，只有一个
	// 因此，以下事件只绑定了一次
	chrome.devtools.network.onRequestFinished.addListener((...args) => {
			channel.postMessage();
	});
}); */



chrome.devtools.network.onRequestFinished.addListener(async (res) => {
	log('请求响应---》', res);
	await handleData(res)
});


chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	// chrome.devtools.inspectedWindow.eval(
	// 	`console.log(${JSON.stringify(allDataList)})`
	// );
	switch (message.source) {
		case 'refresh':
			sendData('devtools-refresh', allDataList)
			break;
		case 'clear':
			allDataList = [];
			sendData('devtools-refresh', allDataList)
			break;

		default:
			break;
	}
});




/**
 * 解密响应数据
 * @param {*} encryptedData
 * @returns
 */
function decrypt(encryptedData) {
	// node.js
	// const key = Buffer.from('yuzhua'.padEnd(32, '\0'), 'binary').toString()

	// 浏览器
	// const str = 'yuzhua'.padEnd(32, '\0')
	// const buffer = new TextEncoder().encode(str)
	// const key = new TextDecoder().decode(buffer)
	try {
		const key = 'yuzhua'.padEnd(32, '\0')
		// 从加密后的字符串中提取 IV 和密文
		const decodedData = CryptoJS.enc.Base64.parse(encryptedData)

		// 获取 IV，并将其与密文分离
		const iv = CryptoJS.lib.WordArray.create(decodedData.words.slice(-4))
		const ciphertext = CryptoJS.enc.Base64.stringify(CryptoJS.lib.WordArray.create(decodedData.words.slice(0, -4)))
		// 使用 AES 解密算法解密数据
		const decryptedText = CryptoJS.AES.decrypt(
			ciphertext,
			CryptoJS.enc.Utf8.parse(key),
			{
				iv: iv,
				mode: CryptoJS.mode.CBC,
				padding: CryptoJS.pad.Pkcs7
			}
		)
		const data = decryptedText.toString(CryptoJS.enc.Utf8)
		return data
		// try {
		// 	return JSON.parse(data)
		// } catch (error) {
		// 	return data
		// }
	} catch (error) {
		console.error('##Exception in data decryption##', error)
		return null
	}
}